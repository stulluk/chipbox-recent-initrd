# chipbox-recent-initrd

Forked from https://github.com/linusw/rootfs-build

Only modified toolchain name and directory according to this toochain: https://gitlab.com/stulluk/chipbox-recent-toolchain

All kudos to Linus Walleij for his amazing works.

## Building

- Clone this repo && cd to chipbox-recent-initrd
```sh
git clone https://gitlab.com/stulluk/chipbox-recent-initrd.git && cd chipbox-recent-initrd
```
- Add gen-init-cpio script to your $PATH
  
```sh
export PATH=$(pwd):$PATH
```

- Make sure above toolchain is setup and ready (Add it to your PATH)

```sh
$  arm-linux-gnueabi-gcc -v
Using built-in specs.
COLLECT_GCC=arm-linux-gnueabi-gcc
COLLECT_LTO_WRAPPER=/home/stulluk/toolchain/bin/../libexec/gcc/arm-linux-gnueabi/8.2.1/lto-wrapper
Target: arm-linux-gnueabi
Configured with: /tmp/dgboter/bbs/rhev-vm2--rhe6x86_64/buildbot/rhe6x86_64--arm-linux-gnueabi/build/src/gcc/configure --target=arm-linux-gnueabi --prefix= --with-sysroot=/arm-linux-gnueabi/libc --with-build-sysroot=/tmp/dgboter/bbs/rhev-vm2--rhe6x86_64/buildbot/rhe6x86_64--arm-linux-gnueabi/build/build-arm-linux-gnueabi/install//arm-linux-gnueabi/libc --with-bugurl=https://bugs.linaro.org/ --enable-gnu-indirect-function --enable-shared --disable-libssp --disable-libmudflap --enable-checking=release --enable-languages=c,c++,fortran --with-gmp=/tmp/dgboter/bbs/rhev-vm2--rhe6x86_64/buildbot/rhe6x86_64--arm-linux-gnueabi/build/build-arm-linux-gnueabi/host-tools --with-mpfr=/tmp/dgboter/bbs/rhev-vm2--rhe6x86_64/buildbot/rhe6x86_64--arm-linux-gnueabi/build/build-arm-linux-gnueabi/host-tools --with-mpc=/tmp/dgboter/bbs/rhev-vm2--rhe6x86_64/buildbot/rhe6x86_64--arm-linux-gnueabi/build/build-arm-linux-gnueabi/host-tools --with-isl=/tmp/dgboter/bbs/rhev-vm2--rhe6x86_64/buildbot/rhe6x86_64--arm-linux-gnueabi/build/build-arm-linux-gnueabi/host-tools --with-arch=armv7-a --with-pkgversion='GNU Toolchain for the A-profile Architecture 8.2-2019.01 (arm-rel-8.28)'
Thread model: posix
gcc version 8.2.1 20180802 (GNU Toolchain for the A-profile Architecture 8.2-2019.01 (arm-rel-8.28)) 
$
```

- Run following 

```sh
./generate-cpio-rootfs chipbox
```

## Result

There resultant cpio binary will be in your $HOME
```sh
$  file ~/rootfs-chipbox.cpio 
/home/stulluk/rootfs-chipbox.cpio: ASCII cpio archive (SVR4 with no CRC)
$
```